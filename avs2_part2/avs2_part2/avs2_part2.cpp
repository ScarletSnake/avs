﻿#include "pch.h"
#include <iostream>
#include <mutex>
#include <atomic>
#include <queue>
#include <chrono>
#include <vector>
#include <condition_variable>

using namespace std;

typedef uint8_t byte;
int tasksCounter;
int completedTasksCounter = 0;
const int TaskNum = 4*1024;

template<typename T>
class Consumer
{
public:
	T* queue;

	Consumer(T* queue)
	{
		this->queue = queue;
	}

	void pop(int ProducerNum)
	{
		byte val = 0;
		bool isPoped;
		while (TaskNum * ProducerNum > completedTasksCounter || queue->size() > 0)
		{
			isPoped = queue->pop(val);
			if (isPoped) {
				tasksCounter += val;
				completedTasksCounter++;
			}
		}
	}
};

template<typename T>
class Producer
{
public:
	T* queue;

	Producer(T* queue)
	{
		this->queue = queue;
	}

	void push()
	{
		for (int i = 0; i < TaskNum; i++)
			queue->push(1);
	}
};

class DynamicMutex
{
private:
	mutex mutex_locker;
	condition_variable popwaiter;

public:
	queue<byte> queue;	

	void push(byte value)
	{
		unique_lock<mutex> locker(mutex_locker);
		queue.push(value);
		popwaiter.notify_one();
	}

	bool pop(byte& value)
	{
		unique_lock<mutex> locker(mutex_locker);
		if (queue.empty()) {
			popwaiter.wait_for(locker, chrono::milliseconds(1));

			if (queue.empty())
				return false;
		}

		value = queue.front();
		queue.pop();
		return true;
	}

	int size()
	{
		return queue.size();
	}
};

class FixedMutex
{
private:
	mutex mutex_locker;
	condition_variable popwaiter;
	condition_variable pushwaiter;
	int queueSize;

public:
	queue<byte> queue;

	FixedMutex(int queueSize)
	{
		this->queueSize = queueSize;
		completedTasksCounter = 0;
	}

	void push(byte value)
	{
		unique_lock<mutex> locker(mutex_locker);
		if (queue.size() == queueSize)
			pushwaiter.wait(locker);

		queue.push(value);
	}

	bool pop(byte& value)
	{
		unique_lock<mutex> locker(mutex_locker);
		if (queue.empty()) {
			popwaiter.wait_for(locker, chrono::milliseconds(1));

			if (queue.empty())
				return false;
		}

		value = queue.front();
		queue.pop();
		pushwaiter.notify_one();
		return true;
	}

	int size()
	{
		return queue.size();
	}
};

template<typename T>
void implementation(T* queue, int ProducerNum)
{
	double time;
	clock_t start = clock();
	vector<thread> producerThreads;
	producerThreads.resize(ProducerNum);

	for (auto& producerThread : producerThreads)
	{
		Producer<T> producer{ queue };
		producerThread = thread(&Producer<T>::push, producer);
	}

	tasksCounter = 0;
	vector<thread> consumerThreads;
	consumerThreads.resize(ProducerNum);

	for (auto& consumerThread : consumerThreads)
	{
		Consumer<T> consumer{ queue };
		consumerThread = thread(&Consumer<T>::pop, consumer, ProducerNum);
	}

	for (auto& producerThread : producerThreads)
		producerThread.join();

	for (auto& consumerThread : consumerThreads)
		consumerThread.join();

	time = clock() - start;

	cout << " - time: " << time << endl;
	cout << " - result: " << tasksCounter << endl << endl;
}

int main()
{
	const int ProducerNums[] = { 1, 2, 4 };
	setlocale(LC_ALL, "Russian");
	for (auto& ProducerNum : ProducerNums)
	{
		cout << "Динамическая очередь" << "\n - ProducerNum: " << ProducerNum << "\n - ConsumerNum: " << ProducerNum << "\n - TaskNum: " << TaskNum << endl;
		DynamicMutex* queue = new DynamicMutex();
		implementation(queue, ProducerNum);

		const int QueueSizes[] = { 1, 4, 16 };
		for (auto& queueSize : QueueSizes)
		{
			cout << "Фиксированная очередь" << "\n - ProducerNum: " << ProducerNum << "\n - ConsumerNum: " << ProducerNum << "\n - TaskNum: " << TaskNum << "\n - queueSize: " << queueSize << endl;
			FixedMutex* queue = new FixedMutex(queueSize);
			implementation(queue, ProducerNum);
		}
	}
}

