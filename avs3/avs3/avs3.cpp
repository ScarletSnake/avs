﻿#include "pch.h"
#include <time.h>
#include <omp.h>
#include <vector>
#include <iostream>
using namespace std;

const int N = 10000;
int* result = new int[N];
int* resultParal = new int[N];

void ParallelSolve(vector<vector<int>> matrix, std::vector<int> vector) {
	clock_t timer;

	timer = clock();

#pragma omp parallel for
	for (int i = 0; i < N; i++)
	{
		int sum = 0;
		for (int j = 0; j < N; j++)
			sum += matrix[i][j] * vector[j];

		resultParal[i] = sum;
	}
	
	cout << "Время с OMP:  " << (long double)clock() - timer << endl;
}

void SimpleSolve(vector<vector<int>> matrix, std::vector<int> vector) {

	int sum = 0;
	clock_t timer;

	timer = clock();

	for (int i = 0; i < N; i++)
	{
		int sum = 0;
		for (int j = 0; j < N; j++)
			sum += matrix[i][j] * vector[j];

		result[i] = sum;
	}

	cout << "\nВремя без OMP:  " << (long double)clock() - timer << endl;
}


int main()
{
	setlocale(LC_ALL, "Russian");
	vector<vector<int>> matrix;
	std::vector<int> vector;

	for (int i = 0; i < N; i++)
	{
		vector.push_back(i);
		std::vector<int> tmp;

		for (int j = 0; j < N; j++)
			tmp.push_back(j);

		matrix.push_back(tmp);
	}

	ParallelSolve(matrix, vector);

	SimpleSolve(matrix, vector);

	bool same = true;
	for (int i = 0; i < N; i++)
	{
		if (result[i] != resultParal[i])
		{
			same = false;
			break;
		}
	}

	if (same)
		cout << "\nОтветы сходятся";
	else
		cout << "\nОтветы не сходятся";
}
