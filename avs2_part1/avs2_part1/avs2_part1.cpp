﻿#include "pch.h"
#include <iostream>
#include <atomic>
#include <mutex>
#include <chrono>
#include <vector>
#include <thread>

using namespace std;

using byte = unsigned short;

mutex mut;
const int NumTasks = 1024*1024;
const int NumThreads[] = { 4, 8, 16, 24 };
std::vector<byte> numTasks;
int mutexIdx = 0;
atomic<int> automicIdx = 0;


template <typename F>
void benchmark(F func) {
	std::vector<bool> bothMethods = { true, false };
	double time;
	clock_t start = clock();
	for (const bool& shouldSleep : bothMethods) {
		clock_t start = clock();

		for (const auto& numThreads : NumThreads) {
			std::vector<thread> thrds;
			thrds.resize(numThreads);

			mutexIdx = 0;
			automicIdx = 0;
			for (auto& thrd : thrds) {
				thrd = thread(func, shouldSleep);
			}

			for (auto& thrd : thrds) {
				thrd.join();
			}
			time = clock() - start;

			cout << "Number of threads: " << numThreads << endl;
			cout << "Sleeping: " << boolalpha << shouldSleep << endl;
			cout << "Time: " << time << endl;
			cout << "Check: " << endl;
			for (auto& numTask : numTasks)
				cout << numTask;
			cout << endl;

			std::fill(numTasks.begin(), numTasks.end(), 0);
		}
	}
}

int main()
{
	bool isAtomic = false;
	numTasks.resize(NumTasks);

	for (int i = 0; i < 2; i++)
	{
		auto globalCounter = [&isAtomic](bool shouldSleep) {
			int counter = 0;
			while (true)
			{
				if (!isAtomic) {
					mut.lock();
					counter = mutexIdx++;
					mut.unlock();
				}

				else
					counter = automicIdx++;

				if (!(counter < NumTasks))
					break;
				numTasks[counter]++;
				if (shouldSleep)
					this_thread::sleep_for(chrono::nanoseconds(10));
			}
			isAtomic = true;
		};

		if(!isAtomic)
			cout << "Mutex: " << endl;
		if (isAtomic)
			cout << "\nAtomic: " << endl;

		benchmark(globalCounter);
	}
}
