﻿#include "pch.h"
#include <iostream>
#include <mmintrin.h>

using namespace std;

struct Sum
{
	__m64 regLeftSum;
	__m64 regRightSum;

	Sum(__m64 newregLeftSum, __m64 newregRightSum)
	{
		regLeftSum = newregLeftSum;
		regRightSum = newregRightSum;
	}

};

Sum Composition(Sum regSumAB, Sum regSumCD)
{
	__m64 regLeftComp;
	__m64 regRightComp;

	regLeftComp = _mm_mullo_pi16(regSumAB.regLeftSum, regSumCD.regLeftSum);
	regRightComp = _mm_mullo_pi16(regSumAB.regRightSum, regSumCD.regRightSum);

	Sum composition = Sum(regLeftComp, regRightComp);

	return composition;
}

Sum getSumAB(__m64 regA, __m64 regB) {

	__m64 m0 = _mm_setzero_si64();
	__m64 mCompration;

	__m64 regLeftSumAB;
	__m64 regRightSumAB;

	__m64 regLeftA;
	__m64 regRightA;
	__m64 regLeftB;
	__m64 regRightB;

	mCompration = _mm_cmpgt_pi8(m0, regA);
	regLeftA = _m_punpcklbw(regA, mCompration);
	regRightA = _m_punpckhbw(regA, mCompration);

	mCompration = _mm_cmpgt_pi8(m0, regB);
	regLeftB = _m_punpcklbw(regB, mCompration);
	regRightB = _m_punpckhbw(regB, mCompration);

	regLeftSumAB = _mm_adds_pi16(regLeftA, regLeftB);
	regRightSumAB = _mm_adds_pi16(regRightA, regRightB);

	Sum sum = Sum(regLeftSumAB, regRightSumAB);

	return sum;
}

Sum getSumCD(__m64 regC, __m64 regLeftD, __m64 regRightD) {

	__m64 m0 = _mm_setzero_si64();
	__m64 mCompration;

	__m64 regLeftC;
	__m64 regRightC;

	mCompration = _mm_cmpgt_pi8(m0, regC);
	regLeftC = _m_punpcklbw(regC, mCompration);
	regRightC = _m_punpckhbw(regC, mCompration);

	__m64 sumLeftCD = _mm_add_pi16(regLeftC, regLeftD);
	__m64 sumRightCD = _mm_add_pi16(regRightC, regRightD);

	Sum sum = Sum(sumLeftCD, sumRightCD);

	return sum;
}

void Solve(__m64 regA,__m64 regB, __m64 regC, __m64 regLeftD, __m64 regRightD)
{
	Sum regSumAB = getSumAB(regA, regB);
	Sum regSumCD = getSumCD(regC, regLeftD, regRightD);

	Sum regComposition = Composition(regSumAB, regSumCD);

	__m64 regLeftCompos = regComposition.regLeftSum;
	__m64 regRightCompos = regComposition.regRightSum;

	for (int i = 0; i < 4; i++) {
		cout << (int)regLeftCompos.m64_i16[i] << " ";
	}

	for (int i = 0; i < 4; i++) {
		cout << (int)regRightCompos.m64_i16[i] << " ";
	}

	cout << "\n";
}

int main()
{

	_int8 A[8] = { 1, 2, 3, 4, 5, 6, 7, 8 };
	_int8 B[8] = { 1, 2, 3, 4, 5, 6, 7, 8 };
	_int8 C[8] = { 1, 2, 3, 4, 5, 6, 7, 8 };
	_int16 LeftD[4] = { 1, 2, 3, 4};
	_int16 RightD[4] = {5, 6, 7, 8 };

	__m64* regA = (__m64*) A;
	__m64* regB = (__m64*) B;
	__m64* regC = (__m64*) C;
	__m64* regLeftD = (__m64*) LeftD;
	__m64* regRightD = (__m64*) RightD;

	Solve(*regA, *regB, *regC, *regLeftD, *regRightD);

	_int8 testA[8] = { -1, -20, 3, 4, 5, 6, 7, 8 };
	_int8 testB[8] = { 1, 2, 3, 4, 5, -16, -70, 8 };
	_int8 testC[8] = { 1, 2, 3, 4, 5, -6, 7, 8 };
	_int16 testLeftD[4] = { 1, 23, 3, 4 };
	_int16 testRightD[4] = { 5, 6, 200, 8 };

	__m64* regTestA = (__m64*) testA;
	__m64* regTestB = (__m64*) testB;
	__m64* regTestC = (__m64*) testC;
	__m64* regTestLeftD = (__m64*) testLeftD;
	__m64* regTestRightD = (__m64*) testRightD;

	Solve(*regTestA, *regTestB, *regTestC, *regTestLeftD, *regTestRightD);

	return 0;
}
